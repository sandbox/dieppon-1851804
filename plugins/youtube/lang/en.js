CKEDITOR.plugins.setLang('youtube', 'en',
{
  youtube : 
  {
    title : "Embed YouTube Video",
    button : "Add YouTube Video",
    pasteMsg : "Please copy and paste the YouTube URL here"
  }
});
