CKEDITOR.plugins.setLang('vimeo', 'en',
{
  vimeo : 
  {
    title : "Embed Vimeo Video",
    button : "Add Vimeo Video",
    pasteMsg : "Please copy and paste the Vimeo URL here"
  }
});
