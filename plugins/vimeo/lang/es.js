CKEDITOR.plugins.setLang('vimeo', 'es',
{
  vimeo : 
  {
    title : "Video Vimeo",
    button : "A&ntilde;ade un video de Vimeo",
    pasteMsg : "Copia y pega aqui tu enlace de Vimeo"
  }
});